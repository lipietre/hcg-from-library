# hcg-from-library

IDPs/IDRs grown via hierarchical assembly of dimer fragments.
Beta version of a HCG web application via Binder.

# Contributions

The code is implemented as described in Pietrek et al., JCTC, 2020, but using dimer fragments as input.
Fragment simulations and initial test were performed by Johannes Betz.
Code development, improvement: Lisa M. Pietrek, Lukas S. Stelzl, Johannes Betz.
Set-up of the Binder enabled Jupyter Notebook together with Klaus Reuter and Sebastian Kehl from the MPCDF.
Special thanks to Dr. Jürgen Köfinger for fruitful discussions and general support.

# References

1 Hierarchical Ensembles of Intrinsically Disordered Proteins at Atomic Resolution in Molecular Dynamics Simulations, Lisa M. Pietrek, Lukas S. Stelzl, and Gerhard Hummer, Journal of Chemical Theory and Computation 2020 16 (1), 725-737, https://pubs.acs.org/doi/abs/10.1021/acs.jctc.9b00809

