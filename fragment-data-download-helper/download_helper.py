#!/usr/bin/env python3

import os
import sys
import glob
import shutil
import tempfile
import subprocess
import configparser


cfg = configparser.ConfigParser()
cfg_file = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        "download_helper.cfg")
cfg.read(cfg_file)
src = cfg["config"]["src"]
git = cfg["config"]["git"]
tar_extra_flags = cfg["config"]["tar_extra_flags"]

out = os.path.join(
    os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..")),
    "dimerLibrary")
# temporary directory prefix for git clone
tmp = os.path.expanduser("~")
# save initial pwd
pwd = os.getcwd()


def make_directory():
    if not os.path.exists(out):
        os.makedirs(out)


def gitlab_download():
    git_dir = tempfile.mkdtemp(dir=tmp)
    try:
        cmd = "git clone --depth=1 {} {}".format(git, git_dir)
        subprocess.check_call(cmd, shell=True)
        file_list = glob.glob(os.path.join(git_dir, "*"))
        dest_list = [os.path.join(out, os.path.basename(file_name)) for file_name in file_list]
        for file_in, file_to in zip(file_list, dest_list):
            shutil.move(file_in, file_to)
    except:
        raise
    finally:
        shutil.rmtree(git_dir)


def download():
    if src == "git":
        gitlab_download()
    else:
        raise RuntimeError("Remote data source (src) must be git") # either git or zenodo.")


def unpack():
    try:
        os.chdir(out)
        file_list = glob.glob("*.tar") + glob.glob("*.tgz") + glob.glob("*.tar.gz")
        for file_name in file_list:
            cmd = "tar -xf {} {}".format(file_name, tar_extra_flags)
            subprocess.check_call(cmd, shell=True)
            os.remove(file_name)
    except:
        raise
    finally:
        os.chdir(pwd)


if __name__ == "__main__":
    make_directory()
    download()
    unpack()

